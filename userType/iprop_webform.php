<?
/**
 * пользовательский тип свойства привязка к вебформам
 * @package default
 * @author (Вернигор Сергей Васильевич)
 */
if (!class_exists("BXPropertyWebform"))
{
    /**
     * Класс для создания пользовательского типа свойства
     */
    class BXPropertyWebform
    {
        function GetUserTypeDescription()
        {
            return array(
                "PROPERTY_TYPE"   => "S",
                "USER_TYPE"                    => "BXPropertyWebform",
                "DESCRIPTION"         => "Привязка к вебформе",
                "GetPropertyFieldHtml"  => array("BXPropertyWebform","GetPropertyFieldHtml"),
                "ConvertToDB"         => array("BXPropertyWebform","ConvertToDB"),
                "ConvertFromDB"   => array("BXPropertyWebform","ConvertFromDB")
            );
        }


        function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
        {
            global $DB;
            $sql = "SELECT ID,NAME FROM b_form";
            $res = $DB->Query($sql);
            $return = "<select name='".htmlspecialchars($strHTMLControlName['VALUE'])."'>";
            $return.="<option value='0'>Веб форма не выбрана</option>";
            while ($arForm = $res->Fetch())
            {
                if ($value["VALUE"]==$arForm["ID"]) $selected = "selected";
                else $selected="";
                $return.="<option $selected value=".$arForm["ID"].">".$arForm["NAME"]."</option>";
            }

            $return .= "</select>";
            return $return;
        }

        function ConvertToDB($arProperty, $value)
        {
            $return = array();
            if(intVal($value["VALUE"]) > 0) $return["VALUE"] = intVal($value["VALUE"]);
            else $return["VALUE"] = "";

            return $return;
        }

        function ConvertFromDB($arProperty, $value)
        {
            $return = array();
            if(intVal($value["VALUE"]) > 0) $return["VALUE"] = intVal($value["VALUE"]);
            else $return["VALUE"] = "";

            return $return;
        }

    }
}
// регистрируем свойство
AddEventHandler("iblock", "OnIBlockPropertyBuildList", Array("BXPropertyWebform", "GetUserTypeDescription"));
AddEventHandler("main", "OnUserTypeBuildList", array("BXPropertyWebform", "GetUserTypeDescriptionMain"));

?>