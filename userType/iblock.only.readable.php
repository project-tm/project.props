<?
class CIBlockPropertyReadable
{
	function GetUserTypeDescription()
	{
		return array(
				"PROPERTY_TYPE" => "S",
				"USER_TYPE" => "ReadableOnly",
				"DESCRIPTION" => "Custom: Строка только для чтения",
				//optional handlers
				"GetPublicEditHTML" => array(__CLASS__,"GetPublicEditHTML"),
				"GetPropertyFieldHtml" => array(__CLASS__,"GetPublicEditHTML"),
			);
	}

	function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
	{
		$html = '<input readonly type="text" id="'.md5($strHTMLControlName["VALUE"]).'" size="'.intval($arProperty["COL_COUNT"]).'" name="'.$strHTMLControlName["VALUE"].'" value="'.htmlspecialchars($value["VALUE"]).'"> ';

		return  $html;
	}

	function GetPublicEditHTML($arProperty, $value, $strHTMLControlName)
	{
		$html = '<input readonly type="text" id="'.md5($strHTMLControlName["VALUE"]).'" size="'.intval($arProperty["COL_COUNT"]).'" name="'.$strHTMLControlName["VALUE"].'" value="'.htmlspecialchars($value["VALUE"]).'"> ';

		return  $html;
	}

}

AddEventHandler("iblock", "OnIBlockPropertyBuildList", Array("CIBlockPropertyReadable", "GetUserTypeDescription"));

?>