<?

/*

  RegisterModuleDependences('iblock', 'OnIBlockPropertyBuildList', 'main', 'CIBlockPropertyCheckbox', 'GetUserTypeDescription', 100, '/php_interface/tools/prop.php');
  RegisterModuleDependences('iblock', 'OnIBlockPropertyBuildList', 'main', 'CIBlockPropertyStringReadOnly', 'GetUserTypeDescription', 100, '/php_interface/tools/prop.php');
  RegisterModuleDependences('iblock', 'OnIBlockPropertyBuildList', 'main', 'CIBlockPropertyOutPrice', 'GetUserTypeDescription', 100, '/php_interface/tools/prop.php');


 */
if (!class_exists("CIBlockPropertyCheckbox")) {

    class CIBlockPropertyCheckbox {

        function GetUserTypeDescription() {
            return array(
                'PROPERTY_TYPE' => 'S',
                'USER_TYPE' => 'Melcosoft_IntReadOnly',
                'DESCRIPTION' => 'CHECKBOX',
                'GetPropertyFieldHtml' => array('CIBlockPropertyCheckbox', 'GetPropertyFieldHtml'),
                'ConvertToDB' => array('CIBlockPropertyCheckbox', 'ConvertToDB'),
                'ConvertFromDB' => array('CIBlockPropertyCheckbox', 'ConvertFromDB')
            );
        }

//\\ function GetUserTypeDescription

        function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName) {
            global $USER;
            $strTemp = "";
            if (intval($value['VALUE']) > 0)
                $strTemp = ' checked="checked"';
            $return = '<input type="checkbox" name="' . $strHTMLControlName['VALUE'] . '" id="' . $strHTMLControlName['VALUE'] . '" value="1"' . $strTemp . '>';
            return $return;
        }

//\\ function GetPropertyFieldHtml

        function ConvertToDB($arProperty, $value) {
            $return = array();
            if (intVal($value['VALUE']) > 0)
                $return['VALUE'] = intVal($value['VALUE']);
            else
                $return['VALUE'] = '';

            return $return;
        }

//\\ function ConvertToDB

        function ConvertFromDB($arProperty, $value) {
            $return = array();
            if (intVal($value['VALUE']) > 0)
                $return['VALUE'] = intVal($value['VALUE']);
            else
                $return['VALUE'] = '';

            return $return;
        }

//\\ function ConvertFromDB
    }

}

if (!class_exists("CIBlockPropertyStringReadOnly")) {

    class CIBlockPropertyStringReadOnly {

        function GetUserTypeDescription() {
            return array(
                'PROPERTY_TYPE' => 'S',
                'USER_TYPE' => 'Melcosoft_StringReadOnly',
                'DESCRIPTION' => '������ readonly',
                'GetPropertyFieldHtml' => array('CIBlockPropertyStringReadOnly', 'GetPropertyFieldHtml'),
                'ConvertToDB' => array('CIBlockPropertyStringReadOnly', 'ConvertToDB'),
                'ConvertFromDB' => array('CIBlockPropertyStringReadOnly', 'ConvertFromDB')
            );
        }

        function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName) {
            CUtil::InitJSCore();
            CJSCore::Init(array("jquery"));
            $strTemp = " readonly";
            $return = '<input type="text" size="40" name="' . $strHTMLControlName['VALUE'] . '" id="' . $strHTMLControlName['VALUE'] . '" value="' . $value['VALUE'] . '"' . $strTemp . '>';
            return $return;
        }

        function ConvertToDB($arProperty, $value) {
            return $value['VALUE'];
        }

        function ConvertFromDB($arProperty, $value) {
            return $value['VALUE'];
        }

    }

}

if (!class_exists("CIBlockPropertyOutPrice")) {

    class CIBlockPropertyOutPrice {

        function GetUserTypeDescription() {
            return array(
                'PROPERTY_TYPE' => 'S',
                'USER_TYPE' => 'Melcosoft_OutPrice',
                'DESCRIPTION' => '���� JQuery',
                'GetPropertyFieldHtml' => array('CIBlockPropertyOutPrice', 'GetPropertyFieldHtml'),
                'ConvertToDB' => array('CIBlockPropertyOutPrice', 'ConvertToDB'),
                'ConvertFromDB' => array('CIBlockPropertyOutPrice', 'ConvertFromDB')
            );
        }

        function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName) {
            CUtil::InitJSCore();
            CJSCore::Init(array("jquery"));
			ob_start();
                       //echo '<pre>'.htmlspecialcharsEx(print_r($value,true)).'</pre>';
?>                        
            <input type="text" size="40" name="<?=$strHTMLControlName['VALUE']?>" id="<?=$strHTMLControlName['VALUE']?>" value="<?=$value['VALUE']?>">
            
<script>
$(function()
{
    price_init();
});

function price_init()
{
    var initial_price = $('[name="<?=$strHTMLControlName['VALUE']?>"]'),
        r_price = $('[name="CAT_BASE_PRICE"]'),
        r_percentage_price = $('[name="PROP[51][n0]"]'),

        ws_price = $('[name="PROP[53][n0]"]'),
        ws_percentage_price = $('[name="PROP[52][n0]"]'),
        sp_price = $('[name="PROP[55][n0]"]'),
        sp_percentage_price = $('[name="PROP[54][n0]"]');

//function money(a){a=str_replace(",",".",a);a=parseFloat(a);isNaN(a)&&(a=0);var b="";0>a&&(b="-");a=Math.abs(a);a=parseInt(100*(a+0.005));z=new String(a/100);0>z.indexOf(".")&&(z+=".00");z.indexOf(".")==z.length-2&&(z+="0");z=b+z;return parseFloat(z)};    
function money(a){a = new String(a); a=a.replace(",",".");a=parseFloat(a);isNaN(a)&&(a=0);var b="";0>a&&(b="-");a=Math.abs(a);a=parseInt(100*(a+0.005));z=new String(a/100);0>z.indexOf(".")&&(z+=".00");z.indexOf(".")==z.length-2&&(z+="0");z=b+z;return parseFloat(z)};    
    
    // ���������� �������� ���� � � ��� �����������
    function price_sh()
    {/*
        if(!parseInt(initial_price.val()))
        {
            initial_price.parent().css({ opacity: .7 });
            r_percentage_price.parent().css({ opacity: .7 });
            ws_percentage_price.parent().css({ opacity: .7 });
            sp_percentage_price.parent().css({ opacity: .7 });
        } else {
            initial_price.parent().css({ opacity: 1 });
            r_percentage_price.parent().css({ opacity: 1 });
            ws_percentage_price.parent().css({ opacity: 1 });
            sp_percentage_price.parent().css({ opacity: 1 });
        }*/
    }


    // ���������� ����
    function recount_initial_price()
    {
        price_sh();
        if(!parseInt(initial_price.val())) return false;
        r_price.val(money(money(initial_price.val()) / 100 * money(r_percentage_price.val()) + money(initial_price.val())));
        ws_price.val(money(money(initial_price.val()) / 100 * money(ws_percentage_price.val()) + money(initial_price.val())));
        sp_price.val(money(money(initial_price.val()) / 100 * money(sp_percentage_price.val()) + money(initial_price.val())));
        ChangeBasePrice(initial_price);
    }

    // ��������� ����
    function recount_r_price()
    {
        if(!parseInt(initial_price.val())) return false;
        r_percentage_price.val(money(money(r_price.val()) / money(initial_price.val()) * 100 - 100));
        ChangeBasePrice(initial_price);
    }

    // ��������� ������� ��������
    function recount_percentage_price()
    {
        if(!parseInt(initial_price.val())) return false;
        r_price.val(money(money(initial_price.val()) / 100 * money(r_percentage_price.val()) + money(initial_price.val())));
        ChangeBasePrice(initial_price);
    }

    // ������� ����
    function recount_ws_price()
    {
        if(!parseInt(initial_price.val())) return false;
        ws_percentage_price.val(money(money(ws_price.val()) / money(initial_price.val()) * 100 - 100));
        ChangeBasePrice(initial_price);
    }

    // ������� ������� ��������
    function recount_ws_percentage_price()
    {
        if(!parseInt(initial_price.val())) return false;
        ws_price.val(money(money(initial_price.val()) / 100 * money(ws_percentage_price.val()) + money(initial_price.val())));
        ChangeBasePrice(initial_price);
    }

    // ���������� ����
    function recount_sp_price()
    {
        if(!parseInt(initial_price.val())) return false;
        sp_percentage_price.val(money(money(sp_price.val()) / money(initial_price.val()) * 100 - 100));
        ChangeBasePrice(initial_price);
    }

    // ���������� ������� ��������
    function recount_sp_percentage_price()
    {
        if(!parseInt(initial_price.val())) return false;
        sp_price.val(money(money(initial_price.val()) / 100 * money(sp_percentage_price.val()) + money(initial_price.val())));
        ChangeBasePrice(initial_price);
    }

    initial_price.keyup(recount_initial_price);
    r_price.keyup(recount_r_price);
    r_percentage_price.keyup(recount_percentage_price);
    ws_price.keyup(recount_ws_price);
    ws_percentage_price.keyup(recount_ws_percentage_price);
    sp_price.keyup(recount_sp_price);
    sp_percentage_price.keyup(recount_sp_percentage_price);

    price_sh();

    return false;
}
</script>
<?            
            $return = ob_get_contents();
            ob_end_clean();
            return $return;
        }

        function ConvertToDB($arProperty, $value) {
		$value["VALUE"] = doubleval($value["VALUE"]);
		if($value["VALUE"] <= 0)
			$value["VALUE"] = "";
		return $value;
        }

        function ConvertFromDB($arProperty, $value) {
		$value["VALUE"] = doubleval($value["VALUE"]);
		if($value["VALUE"] <= 0)
			$value["VALUE"] = "";
		return $value;
        }

    }

}

?>