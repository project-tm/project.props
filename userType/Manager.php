<?

namespace QbChat\Property;

class Manager extends \CUserTypeInteger {

    // инициализация пользовательского свойства для главного модуля
    function GetUserTypeDescription() {
        return array(
            "USER_TYPE_ID" => "QbChatManager",
            "CLASS_NAME" => __CLASS__,
            "DESCRIPTION" => "QbChat: Менеджеры",
            "BASE_TYPE" => "int",
        );
    }

    // инициализация пользовательского свойства для инфоблока
    function GetIBlockPropertyDescription() {
        return array(
            "PROPERTY_TYPE" => "S",
            "USER_TYPE" => "QbChatManager",
            "DESCRIPTION" => "QbChat: Менеджеры",
            'GetPropertyFieldHtml' => array(__CLASS__, 'GetPropertyFieldHtml'),
            'GetAdminListViewHTML' => array(__CLASS__, 'GetAdminListViewHTML'),
        );
    }

    // представление свойства
    function getViewHTML($name, $value) {
        if($value) {
            $arUser = \QbChat\User::getData($value);
            return $arUser['NAME'];
        } else {
            return 'Не выбрано';
        }
    }

    // редактирование свойства
    function getEditHTML($name, $value, $is_ajax = false) {
        $arUsers = \QbChat\User::getList(\QbChat\Chat::getGroupManager());
        $result = '<select id="' . $name . '" name="' . $name . '" style="width:250px;"><option value="0">Не выбрано</option>';
        foreach (\QbChat\User::getList(\QbChat\Chat::getGroupManager()) as $userId => $name) {
            $result .= '<option value="' . $userId . '" ' . ($value == $userId ? 'selected="selected"' : '') . '>' . $name . '</option>';
        }
        return $result . '</select>';
    }

    // редактирование свойства в форме (главный модуль)
    function GetEditFormHTML($arUserField, $arHtmlControl) {
        return self::getEditHTML($arHtmlControl['NAME'], $arHtmlControl['VALUE'], false);
    }

    // редактирование свойства в списке (главный модуль)
    function GetAdminListEditHTML($arUserField, $arHtmlControl) {
        return self::getViewHTML($arHtmlControl['NAME'], $arHtmlControl['VALUE'], true);
    }

    // представление свойства в списке (главный модуль, инфоблок)
    function GetAdminListViewHTML($arProperty, $value, $strHTMLControlName) {
        return self::getViewHTML($strHTMLControlName['VALUE'], $value['VALUE']);
    }

    // редактирование свойства в форме и списке (инфоблок)
    function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName) {
        return $strHTMLControlName['MODE'] == 'FORM_FILL' ? self::getEditHTML($strHTMLControlName['VALUE'], $value['VALUE'], false) : self::getViewHTML($strHTMLControlName['VALUE'], $value['VALUE']);
    }

}
