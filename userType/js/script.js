$(function() {
	$('.colorSelector3').on('mouseenter', function() {
		var color = "#";
		color = color + $(this).parent().find("#color").val();
		$(this).ColorPicker({
			color: color,
			onShow: function(colpkr) {
				$(colpkr).fadeIn(5);
				return false;
			},
			onHide: function(colpkr) {
				$(colpkr).fadeOut(5);
				return false;
			},
			onSubmit: function(hsb, hex, rgb, el) {
				$(el).find("div").css("backgroundColor", "#" + hex);
				$(el).parent().find("#color").val(hex);
				$(el).ColorPickerHide();
			}
		});
	});
});