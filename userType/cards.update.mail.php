<?

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');


$arResult = array();
if ($USER->IsAdmin() and ! empty($_GET['ID'])) {
    $DB->StartTransaction();
    $isOk = false;
    $ID = (int) $_GET['ID'];
    foreach (\Local\Helpers\Applications::getDataPgf(array('ID' => $ID)) as $arItem) {
        if (empty($arItem['CODE'])) {
            $code = \Local\Helpers\Applications::sendUserUpdateMail($ID, $arItem);
            $isOk = true;
        } else {
            $arResult['error'] = '<span style="color: red;">Пользователю уже отправлено письмо</span>';
        }
    }
    if ($isOk) {
        $DB->Commit();
        $arResult['message'] = 'Пользователю отправлено письмо<br>Страница редактирования: ' . "http://{$_SERVER['SERVER_NAME']}/register/{$ID}/{$code}/";
//        $arResult['message'] = 'Пользователю отправлено письмо';
    } else {
        $DB->Rollback();
    }
}

if (empty($arResult['error'])) {
    $arResult['success'] = true;
}
header('Content-Type: application/json');
echo json_encode($arResult);

