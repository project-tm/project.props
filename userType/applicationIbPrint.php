<?php

class applicationIbPrint {

    function addHeaders() {
        static $is = false;
        if ($is)
            return;
        $is = true;
        global $APPLICATION;
        $APPLICATION->AddHeadScript('/bitrix/js/main/jquery/jquery-1.8.3.min.js');
        ob_start();
        ?>
        <script type="text/javascript">
            $(document).ready(function () {
                $(document).on('click', '.vashoutlet-print', function () {
                    var data = $(this).closest('tr').attr('oncontextmenu');
                    data = 'data = function() {' + data + '};';
                    eval(data);
                    var d = data();
                    var reg = /(^.+type=anketa1&ID=)([0-9]+)(.+)/gi;
                    var ID = d[0].ONCLICK.replace(reg, "$2");
                    if (ID) {
                        $(this).attr({href: "/bitrix/admin/print_pdf.php?ID=" + ID});
                    }
                }).on('dblclick', '.vashoutlet-print', function () {
                    return false;
                });
            });
        </script>                
        <?

        $APPLICATION->AddHeadString(ob_get_clean());
    }

    function GetUserTypeDescription() {
        return array(
            "USER_TYPE_ID" => "print",
            "CLASS_NAME" => __CLASS__,
            "DESCRIPTION" => "Распечатать [vashoutlet.ru]",
            "BASE_TYPE" => "int",
        );
    }

    function GetIBlockPropertyDescription() {
        return array(
            "PROPERTY_TYPE" => "S",
            "USER_TYPE" => "print",
            "DESCRIPTION" => "Распечатать [vashoutlet.ru]",
            'GetPropertyFieldHtml' => array(__CLASS__, 'GetPropertyFieldHtml'),
            'GetAdminListViewHTML' => array(__CLASS__, 'GetAdminListViewHTML'),
        );
    }

    function getViewHTML($name, $value) {
        return "<div><a href='' class='vashoutlet-print' target='_blank'>Распечатать</a></div>";
    }

    function getEditHTML($name, $value, $is_ajax = false) {
        return '';
    }

    function getAdminEditHTML($name, $value, $is_ajax = false) {
        return '';
    }

    function GetEditFormHTML($arUserField, $arHtmlControl) {
        return self::getEditHTML($arHtmlControl['NAME'], $arHtmlControl['VALUE'], false);
    }

    function GetAdminListEditHTML($arUserField, $arHtmlControl) {
        self::addHeaders();
        return self::getViewHTML($arHtmlControl['NAME'], $arHtmlControl['VALUE'], true);
    }

    function GetAdminListViewHTML($arProperty, $value, $strHTMLControlName) {
        self::addHeaders();
        return self::getViewHTML($strHTMLControlName['VALUE'], $value['VALUE']);
    }

    function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName) {
        $field = self::getAdminEditHTML($strHTMLControlName['VALUE'], $value['VALUE'], false);
        return $field;
    }

}
?>