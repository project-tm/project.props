<?php

namespace Local\UserType;

class CardRegister {

    function addHeaders() {
        static $is = false;
        if ($is)
            return;
        $is = true;
        global $APPLICATION;
        $APPLICATION->AddHeadScript('/bitrix/js/main/jquery/jquery-2.1.3.min.min.js');
        \CJSCore::Init(array('window'));
        ob_start();
        ?>
        <script type="text/javascript">
            $(function () {
                var obPopupWin = function (title, content) {
                    console.log(title, content);
                    var item = (new BX.CDialog({
                        width: 500,
                        height: 70,
                        title: title,
                        content: content,
                        name: 'CARDS_UPDATE_MAIL',
                        id: 'CARDS_UPDATE_MAIL',
                        action: function () {
                            this.Close();
                        },
                        onclick: "BX.WindowManager.Get().Close()"
                    }));
//                    item.SetHead(content);
                    item.Show();
                }
                $(document).on('click', '.metro-card-register', function () {
                    var ID = 0;
                    if ($(this).closest('tr.adm-list-table-row').is('tr')) {
                        var data = $(this).closest('tr.adm-list-table-row').attr('ondblclick');
                        ID = data.replace(/(^.+&ID=)([0-9]+)(.+)/gi, '$2');
                    } else if ($(this).closest('td.adm-detail-content-cell-r').is('td')) {
                        ID = $(this).closest('form').find('input[name="ID"]').val();
                    }
                    if (ID && confirm("Вы уверены?")) {
                        console.log(ID);
                        $.get('/local/tools/cards.update.mail.php', {ID: ID}, function (res) {
                            console.log(res);
                            if (res.success) {
                                obPopupWin('Запрос отправлен', res.message);
                            } else {
                                obPopupWin('Возникла ошибка', res.error);
                            }
                        }, 'json').error(function () {
                            obPopupWin('Возникла ошибка', 'Ошибка сервера, повторите запрос позднее');
                        });
                    }
                }).on('dblclick', '.metro-card-register', function () {
                    return false;
                });
            });
        </script>
        <style>
            .metro-card-register {
                cursor: pointer;
            }
        </style>
        <?

        $APPLICATION->AddHeadString(ob_get_clean());
    }

    function GetUserTypeDescription() {
        return array(
            "USER_TYPE_ID" => "CardRegister",
            "CLASS_NAME" => __CLASS__,
            "DESCRIPTION" => "Отправить на дозаполнением клиентом [metro]",
            "BASE_TYPE" => "int",
        );
    }

    function GetIBlockPropertyDescription() {
        return array(
            "PROPERTY_TYPE" => "S",
            "USER_TYPE" => "CardRegister",
            "DESCRIPTION" => "Отправить на дозаполнением клиентом [metro]",
            'GetPropertyFieldHtml' => array(__CLASS__, 'GetPropertyFieldHtml'),
            'GetAdminListViewHTML' => array(__CLASS__, 'GetAdminListViewHTML'),
        );
    }

    function getViewHTML($name, $value) {
        self::addHeaders();
        return "<div><a class='metro-card-register'>Отправить</a></div>";
    }

    function getEditHTML($name, $value, $is_ajax = false) {
        return self::getViewHTML($name, $value, false);
    }

    function getAdminEditHTML($name, $value, $is_ajax = false) {
        return self::getViewHTML($name, $value, false);
    }

    function GetEditFormHTML($arUserField, $arHtmlControl) {
        return self::getEditHTML($arHtmlControl['NAME'], $arHtmlControl['VALUE'], false);
    }

    function GetAdminListEditHTML($arUserField, $arHtmlControl) {
        return self::getViewHTML($arHtmlControl['NAME'], $arHtmlControl['VALUE'], true);
    }

    function GetAdminListViewHTML($arProperty, $value, $strHTMLControlName) {
        return self::getViewHTML($strHTMLControlName['VALUE'], $value['VALUE']);
    }

    function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName) {
        return self::getAdminEditHTML($strHTMLControlName['VALUE'], $value['VALUE'], false);
    }

}
?>